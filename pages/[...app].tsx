import { useEffect, useState } from "react";
import CreateReactAppRoot from "../src/App";
import useSWR from "swr";
import { Data } from "./api/auth";
import axios from "axios";

const fetcher = (url: string) => fetch(url).then((res) => res.json());

interface Props {
  data: Data;
}
const App = ({ data }: Props) => {
  const [isMounted, setIsMounted] = useState(false);
  useEffect(() => {
    setIsMounted(true);
  }, []);

  // const { data, error } = useSWR(
  //   "https://api.github.com/repos/vercel/swr",
  //   fetcher
  // );

  // if (error) return "An error has occurred.";
  // if (!data) return "Loading...";
  // console.log("data", data);

  // Don't render client-side app unless window is available.
  // NOTE: Page not statically pre-rendered nor server-side renderable
  return isMounted ? <CreateReactAppRoot /> : <div>Empty</div>;
};

export async function getServerSideProps() {
  // const result = await fetch("http://localhost:3000/api/auth");
  // const data = await result.json();
  return { props: { } };
}

export default App;
