import type { NextApiRequest, NextApiResponse } from 'next'

export type Data = {
  access_token: string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  // Get access_token from auth service
  res.status(200).json({ access_token: `token value` })
}
